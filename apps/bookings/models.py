from django.db import models
from apps.cars.models import *
from apps.customers.models import *
from carrental.base.models import BaseModel



class Booking(BaseModel):
    
    start_date = models.DateTimeField()

    end_date = models.DateTimeField()

    car_selected = models.ForeignKey(Car, on_delete=models.SET_NULL, null=True, blank=True)

    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True, blank=True)

    state = models.IntegerField(default=0)

    class Meta:
        verbose_name = "Reserva"
        verbose_name_plural = "Reservas"

    def __str__(self):
        return "{0} {1} - {2} a {3} - {4}".format(self.customer.user.first_name
        ,self.customer.user.last_name
        ,self.start_date
        ,self.end_date
        ,self.car_selected)


class BookingObservation(BaseModel):
    
    booking = models.ForeignKey(Booking, on_delete=models.CASCADE)
    comment = models.TextField()


    class Meta:
        verbose_name = "Observación en Renta"
        verbose_name_plural = "Observaciones en Rentas"

    def __str__(self):
        return "{0} - {1}".format(self.booking
        ,self.booking.customer)
