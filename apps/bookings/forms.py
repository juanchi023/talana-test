from django import forms
from .models import *
from bootstrap_datepicker_plus import DateTimePickerInput



class BookingCreationForm(forms.ModelForm):

	class Meta:
		model = Booking
		fields = ('start_date',
        'end_date',
        )

		widgets = {
			'start_date': DateTimePickerInput(format='%d/%m/%Y %H:%M')
			,'end_date': DateTimePickerInput(format='%d/%m/%Y %H:%M')
        }