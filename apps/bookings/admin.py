from django.contrib import admin

from .models import *

@admin.register(Booking,BookingObservation)
class BookingAdmin(admin.ModelAdmin):
    pass
