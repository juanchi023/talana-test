from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.contrib.auth import login,logout, authenticate
from .models import *
from .forms import *
from apps.bookings.forms import *
from apps.bookings.models import *
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db.models import Q
try: 
    from django.utils import simplejson as json
except ImportError:
    import json  
from django.views.decorators.http import require_POST
from datetime import date, datetime



def home_view(request):

	if not request.user.is_anonymous:
		return HttpResponseRedirect(reverse('panel_view'))
	else:
		form = EmailAuthenticationForm(request.POST or None)
	
		if form.is_valid():
			login(request, form.get_user())
			return HttpResponseRedirect(reverse('panel_view'))

	return render(request,'portal/login.html',{'form':form})


@login_required
def logout_view(request):
    logout(request)
    return HttpResponseRedirect(reverse('home_view'))


@login_required
def panel_view(request):

    return render(request,'portal/panel.html',)


@login_required
def booking_list(request):

    return render(request,'portal/bookings.html',)


@login_required
def new_booking(request):
	
	form = BookingCreationForm(request.POST or None)
	
	return render(request,'portal/new-booking.html',{'form':form})