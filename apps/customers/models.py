from django.db import models
from django.contrib.auth.models import User



class Customer(models.Model):

    ID_T = (
        ('CC', 'Cédula de Identidad')
        , ('PS', 'Pasaporte')
        , ('CE','Cédula de Extranjería')
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='customer')
    phone = models.CharField(max_length=15, blank=True)    
    id_type = models.CharField(max_length=25, choices=ID_T, blank=True)
    id_number = models.CharField(max_length=25, null=True, blank=True)
    points = models.BigIntegerField(default=0)
    

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"

    def __str__(self):
        return "{0} {1} - {2}".format(self.user.first_name
        ,self.user.last_name
        ,self.user.email)


