from django.urls import include, path
from apps.customers import views as customers_views

urlpatterns = [

    path('', customers_views.home_view
        , name='home_view'),

    path('panel/', customers_views.panel_view
        , name='panel_view'),

    path('logout/', customers_views.logout_view
        , name='logout_view'),

    path('panel/reservas/', customers_views.booking_list
        , name='booking_list'),

    path('panel/reservas/nueva/', customers_views.new_booking
        , name='new_booking'),       

        
]