from django.db import models
from carrental.base.models import BaseModel



class Car(BaseModel):

    CAR_TYPE = (('Descapotable', 'Descapotable')
    , ('Económico', 'Económico')
    , ('Cupé', 'Cupé')
    , ('Hatchback','Hatchback')
    , ('Roadster', 'Roadster')
    , ('Familiar', 'Familiar')
    , ('Todoterreno', 'Todoterreno')
    , ('Crossover', 'Crossover')
    , ('Deportivo', 'Deportivo')
    , ('Pick-up', 'Pick-up')
    )
    
    vin = models.CharField(max_length=17)
    brand = models.CharField(max_length=255, null=True, blank=True)
    model = models.CharField(max_length=255, null=True, blank=True)
    year = models.IntegerField(null=True, blank=True)
    type = models.CharField(max_length=15, choices=CAR_TYPE, blank=True)
    number_of_doors = models.IntegerField(null=True, blank=True)
    is_diesel = models.BooleanField(default=False)



    class Meta:
        verbose_name = "Auto"
        verbose_name_plural = "Autos"

    def __str__(self):
        return "{0} - {1} - {2} - {3}".format(self.vin
        ,self.brand
        ,self.model
        ,self.type)